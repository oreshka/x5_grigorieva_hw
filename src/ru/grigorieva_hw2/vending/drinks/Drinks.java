package drinks;

//AG Вместо интерфейса используем перечисление
// Напишем конструктор класса
public enum Drinks {

    TEA("Чай", 150),
    COFFEE("Кофе", 300),
    WATER("Вода", 40),
    CACAO("Какао", 150),
    COLA("Кола", 50);

    private String title;
    private double price;

    Drinks(String title, double price) {
        this.title = title;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }
}
