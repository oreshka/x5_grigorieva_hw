import drinks.Drinks;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        // Исходно в автомат грузим колу и чай
        VendingMachine vm = new VendingMachine(Arrays.asList(Drinks.COLA, Drinks.TEA));
        // хочу колу, но жадный автомат требует денег
        vm.giveMeADrink(0, 0);
        // приходится платить
        vm.giveMeADrink(0, 200);
        // На сдачу беру чай
        vm.giveMeADrink(1, 200);
        // пробую взять еще что-нибудь
        vm.giveMeADrink(2, 500);
        // догружаю в автомат напитки
        vm.addDrinks(Arrays.asList(Drinks.CACAO, Drinks.COFFEE, Drinks.WATER));
        // беру воду
        vm.giveMeADrink(4, 500);
    }
}