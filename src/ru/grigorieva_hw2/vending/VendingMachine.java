import drinks.Drinks;

import java.util.ArrayList;
import java.util.List;
// AG добавим сообщений пользователю

public class VendingMachine {
    private List<Drinks> drinks = new ArrayList<>();
    private int index = 0;

    public VendingMachine() {
        System.out.println("Привезли новый вендинговый автомат!!!");
    }

    public VendingMachine(List<Drinks> drinks) {
        System.out.println("Привезли новый вендинговый автомат!!!");
        addDrinks(drinks);
    }

    private Drinks getDrink(int key) {
        if (key >= 0 && key < drinks.size()){
            return drinks.get(key);
        } else {
            return null;
        }
    }

    public void giveMeADrink(int key, double money) {
        System.out.println(String.format("Запрошен напиток с номером %d. Передано %f рублей", key, money));
        if (money > 0) {
            Drinks drink = getDrink(key);

            if (drink != null) {
                if (drink.getPrice() <= money) {
                    System.out.println(String.format("Возьмите ваш напиток: %s, и сдачу %f", drink.getTitle(), money - drink.getPrice()));
                } else {
                    System.out.println(String.format("Недостаточно средств: требуется %f, передано %f", drink.getPrice(), money));
                }
            } else {
                System.out.println(String.format("Нет такого напитка! Номер %d", key));
            }
        } else {
            System.out.println("Бесплатно не работаем!");
        }
    }

    public void addDrinks(List<Drinks> drinks) {
        drinks.forEach(drink -> System.out.println(String.format("Добавлен напиток %s по цене %f. Номер напитка %d", drink.getTitle(), drink.getPrice(), index++)));
        this.drinks.addAll(drinks);
    }
}
