package ru.grigorieva_hw5_unique;

/*AG допишем и переопределим методы, используем коллекцию Map

 */

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class UniqueChars {

    private Map<Character, Integer> textMap = new HashMap<>();

    private String text;

    public String getText() {
        StringBuilder result = new StringBuilder();
        textMap.entrySet().forEach(new Consumer<Map.Entry<Character, Integer>>() {
            @Override
            public void accept(Map.Entry<Character, Integer> item) {
                result.append(item.getKey()).append(" - ").append(item.getValue()).append("\n");
            }
        });
        return result.toString();
    }

    public void setText(String text) {
        this.text = text;
    }

    public void calculate() {
        textMap.clear();
        for (int i =0; i < text.length(); i++) {
            Integer count = textMap.get(text.charAt(i));
            if (count == null) {
                textMap.put(text.charAt(i), 1);
            } else {
                textMap.put(text.charAt(i), ++count);
            }
        }
    }
}
