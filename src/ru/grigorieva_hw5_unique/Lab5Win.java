package ru.grigorieva_hw5_unique;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Lab5Win {

    /*
    AG Исходный файл
     */

    private JFrame frmLab;
    private UniqueChars uniqueChars;
    private JTextField textField;
    private JTextArea textArea;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                Lab5Win window = new Lab5Win();
                window.frmLab.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the application.
     */
    public Lab5Win() {
        uniqueChars = new UniqueChars();
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmLab = new JFrame();
        frmLab.setTitle("Lab 4 - unique chars");
        frmLab.setBounds(100, 100, 326, 365);
        frmLab.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmLab.getContentPane().setLayout(null);

        textField = new JTextField();
        textField.setText("Enter some text");
        textField.selectAll();
        textField.setBounds(36, 40, 234, 23);
        frmLab.getContentPane().add(textField);

        textArea = new JTextArea();
        textArea.setBounds(36, 133, 234, 181);
        frmLab.getContentPane().add(textArea);

        JButton btnCalc = new JButton("Calc()");
        btnCalc.addActionListener(e -> {
            uniqueChars.setText(textField.getText());
            uniqueChars.calculate();
            textArea.setText(uniqueChars.getText());
        });
        btnCalc.setBounds(180, 80, 89, 23);
        frmLab.getContentPane().add(btnCalc);
    }
}
