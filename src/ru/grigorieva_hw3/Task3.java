package ru.grigorieva_hw3;

public class Task3 {
    //AG Заставить программу Hello World упасть
    // Смоделировав ошибку NullPointerException
    // Смоделировав ошибку ArrayIndexOutOfBoundsException
    // Вызвав свой вариант ошибки через оператор throw

    /**
     * Created by arty on 01.12.2018.
     */

    /**
     * Основной метод, с которого начинается
     * выполнение любой Java программы.
     */
    private static int[] myArray = new int[2];
    private static int myExc;

    public static void main(String args[]) {

        System.out.println("Hello, world!");
        System.out.printf("ArrayIndexOutOfBoundsException ", myArray[3]);

        if (myExc == 0) {
            throw new NullPointerException("NullPointerException");
        }

        try {
            myArray[1]=1/0;

        } catch (ArithmeticException e) {
            System.out.println("Арифметическая ошибка деления на ноль");

        }


    }
}