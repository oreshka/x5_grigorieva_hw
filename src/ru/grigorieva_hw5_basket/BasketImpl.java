package ru.grigorieva_hw5_basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
AG Зададим коллекцию HashMap и переопределим исходные методы
 */

public class BasketImpl implements ru.grigorieva_hw5_basket.Basket {

    private Map<String, Integer> basket = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {
        if (quantity < 0) {
            System.out.println("Количестов не может быть отрицательным");
            return;
        }
        basket.merge(product, quantity, (quantity1, current_quantity) -> quantity1 + current_quantity);
    }

    @Override
    public void removeProduct(String product) {
        basket.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        if (quantity < 0) {
            System.out.println("Количестов не может быть отрицательным");
            return;
        }
        basket.put(product, quantity);
    }

    @Override
    public void clear() {
        basket.clear();
    }

    @Override
    public List<String> getProducts() {
        return new ArrayList<>(basket.keySet());
    }

    @Override
    public int getProductQuantity(String product) {
        Integer quantity = basket.get(product);
        if (quantity == null) {
            return 0;
        }
        return  basket.get(product);
    }

    public static void main(String[] args) {
        ru.grigorieva_hw5_basket.Basket basket = new BasketImpl();
        basket.addProduct("Вода", -5);
        basket.addProduct("Молоко", 5);
        basket.updateProductQuantity("Вода", 100);
        basket.removeProduct("Молоко");
        System.out.println(basket.getProducts());
        System.out.println(basket.getProductQuantity("Вода"));
        System.out.println(basket.getProductQuantity("Молоко"));
    }

}
