package ru.grigorieva_hw5_basket;

/*
AG исходный интерфейс из описания задачи
 */

import java.util.List;

public interface Basket {

    void addProduct(String product, int quantity);

    void removeProduct(String product);

    void updateProductQuantity(String product, int quantity);

    void clear();

    List<String> getProducts();

    int getProductQuantity(String product);
}
