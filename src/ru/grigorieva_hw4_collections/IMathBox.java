package ru.grigorieva_hw4_collections;

import ru.grigorieva_hw4_collections.annotation.ClearData;

import java.util.*;

public interface IMathBox {
    void add(Long digit);

    boolean remove(Long digit);

    Long getTotal();

    double getAverage();

    Set<Long> getMultiplied(int multiplicator);

    Map<Integer, String> getMap();

    Long getMin();

    Long getMax();
}
