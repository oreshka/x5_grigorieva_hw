package ru.grigorieva_hw4_collections;
/*ДЗ_4+8
        Написать класс MathBox, реализующий следующий функционал:
        1.    Конструктор на вход получает массив Number. Элементы не могут повторяться. Элементы массива внутри объекта раскладываются в подходящую коллекцию (выбрать самостоятельно).
        2.    Существует метод summator, возвращающий сумму всех элементов коллекции
        3.    Существует метод splitter, выполняющий поочередное деление всех хранящихся в объекте элементов на делитель, являющийся аргументом метода. Хранящиеся в объекте данные полностью заменяются результатами деления.
        4.    Необходимо правильно переопределить методы toString, hashCode, equals, чтобы можно было использовать MathBox для вывода данных на экран и хранение объектов этого класса в коллекциях (например, hashMap). Выполнение контракта обязательно!
        5.    В будущем может возникнуть необходимость расширения функционала класса. Например, замена хранящихся элементов по значению.
*/
import java.lang.reflect.Proxy;

public class Test {

    public static void main(String... args) {
        Mathbox mathbox = new Mathbox();
        mathbox.add(100L);
        mathbox.add(35L);
        mathbox.remove(44L);
        mathbox.remove(100L);
        mathbox.add(90L);
        print(mathbox);
        mathbox.getMultiplied(2);
        print(mathbox);

        CustomInvocationHandler invocationHandler = new CustomInvocationHandler(mathbox);

        IMathBox proxy = (IMathBox) Proxy.newProxyInstance(Mathbox.class.getClassLoader(), Mathbox.class.getInterfaces(), invocationHandler);;

        print(proxy);
        proxy.add(200L);
        print(proxy);
        proxy.remove(0L);
        print(proxy);
    }

    private static void print(IMathBox mathbox) {
        System.out.println(mathbox.getAverage());
        System.out.println(mathbox.getMax());
        System.out.println(mathbox.getMin());
        System.out.println(mathbox.getTotal());
        System.out.println(mathbox.getMap());
    }
}
