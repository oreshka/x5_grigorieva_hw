package ru.grigorieva_hw4_collections;

import ru.grigorieva_hw4_collections.annotation.ClearData;
import ru.grigorieva_hw4_collections.annotation.LogData;
import ru.grigorieva_hw4_collections.annotation.ClearData;
import ru.grigorieva_hw4_collections.annotation.LogData;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashSet;

public class CustomInvocationHandler implements InvocationHandler {

    private ru.grigorieva_hw4_collections.Mathbox mathbox;

    public CustomInvocationHandler(ru.grigorieva_hw4_collections.Mathbox mathbox) {
        this.mathbox = mathbox;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        ClearData clearData = mathbox.getClass().getMethod(method.getName(), method.getParameterTypes()).getAnnotation(ClearData.class);
        LogData logData = mathbox.getClass().getMethod(method.getName(), method.getParameterTypes()).getAnnotation(LogData.class);
        printData(logData);
        Object result = method.invoke(mathbox, args);
        printData(logData);
        if (clearData != null) {
            Field field = mathbox.getClass().getDeclaredField("digits");
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            field.set(mathbox, new HashSet<Long>());
            field.setAccessible(accessible);
        }

        return result;
    }

    private void printData(LogData logData) throws NoSuchFieldException, IllegalAccessException {
        if (logData != null) {
            Field field = mathbox.getClass().getDeclaredField("digits");
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            System.out.println(field.get(mathbox));
            field.setAccessible(accessible);
        }
    }
}
