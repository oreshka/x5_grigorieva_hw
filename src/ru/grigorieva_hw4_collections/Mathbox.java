package ru.grigorieva_hw4_collections;

import ru.grigorieva_hw4_collections.annotation.ClearData;
import ru.grigorieva_hw4_collections.annotation.LogData;

import java.util.*;

public class Mathbox implements IMathBox {

    private Set<Long> digits = new HashSet<>();

    @LogData
    public void add(Long digit) {
        digits.add(digit);
    }

    @ClearData
    public boolean remove(Long digit) {
        return digits.remove(digit);
    }

    public Long getTotal() {
       return  digits.stream().mapToLong(l -> l).sum();
    }

    public double getAverage() {
        OptionalDouble average = digits.stream().mapToLong(l -> l).average();
        return average.isPresent() ? average.getAsDouble() : 0;
    }

    public Set<Long> getMultiplied(int multiplicator) {
        if (multiplicator == 0) {
            throw new IllegalArgumentException("Не буду умножать на 0!!!");
        }
        Set<Long> result = new HashSet<>();
        digits.forEach(item -> {
            result.add(item * multiplicator);
        });
        digits.clear();
        digits.addAll(result);
        return result;
    }

    public Map<Integer, String> getMap() {
        Map<Integer, String> result = new HashMap<>();
        digits.forEach(item -> result.put(item.intValue(), item.toString()));
        return result;
    }

    public Long getMin() {
        OptionalLong result = digits.stream().mapToLong(l -> l).min();
        return result.isPresent() ? result.getAsLong() : 0;
    }

    public Long getMax() {
        OptionalLong result = digits.stream().mapToLong(l -> l).max();
        return result.isPresent() ? result.getAsLong() : 0;
    }
}
