package ru.grigorieva;

import java.util.Scanner;

public class Task1_1 {
//AG 1.1 Программа, считающая стоимость бензина
// (на вход программе передается кол-во литров, на выходе печатается стоимость)
    public static void main(String[] args) {
        double result = 53.33;
        Scanner in = new Scanner(System.in);

        System.out.println("Введите количество литров (целое число): ");
        int fuelCount = in.nextInt();
        System.out.printf("Спасибо. Бензин сегодня стоит %.2f \n", result);
        System.out.printf("Стоимость вашей покупки: %.2f", fuelCount*result);
    }
}
