package ru.grigorieva;

import java.util.Scanner;

public class Task1_3 {
    //AG Написать программу для вывода на экран таблицы умножения
    // до введенного пользователем порога
    private static int[] prTable = new int[11];

    public static void main(String[] args) {
        System.out.println("Введите целое число от 1 до 10: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int j=1; j<=n; j++){
            makePrTable(j);
            System.out.println();

        }

    }

    private  static void makePrTable(int n)
    {
        for (int i=1; i < 11; ++i) {
            prTable[i] = n * i;
            System.out.printf("%d x %d = %d\n", n, i, prTable[i]);
        }

    }

}
