package ru.grigorieva;

import java.util.Scanner;

public class Task1_4 {
    //AG Написать программу, вычисляющую факториал N
    public static void main(String[] args) {
        System.out.println("Введите целое число N: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.printf("Факториал N = %d \n", Fact(n));
    }

    private static int Fact(int n){
        int result;
        if(n == 1) {
            return 1;
        }
        result = Fact(n - 1) * n;
        return result;
        }


    }

