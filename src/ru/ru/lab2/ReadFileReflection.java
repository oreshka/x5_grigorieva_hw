package ru.ru.lab2;
/*

ДЗ_Урок15
Задание 2 (пакет 2)

 Написать программу, которая выполняет сохранение в файл и чтение из файла простого
 (содержащего только поля-примитивы и String) объекта.
  Не использовать для этого ObjectOutput и ObjectInput (реализовать механизм сериализации-десериализации самостоятельно).
  Скорее всего, вам пригодится рефлексия (reflections API).
*/
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.io.BufferedReader;
import java.io.FileReader;

public class ReadFileReflection {

    //примитивы
    public int test1 = 0;
    public String test2 = "hello";
    Boolean test3 = false;
    int test4 = 1;

    //результат
    StringBuilder result = new StringBuilder();

    public static void main(String[] args) throws IOException {

        ReadFileReflection test = new ReadFileReflection();
        test.ReadFile();
        test.WriteFile();

    }


    public String toString() {


        final String NewLine = System.getProperty("line.separator");

        result.append(this.getClass().getName());
        result.append(" Class {");
        result.append(NewLine);


        Field[] fields = this.getClass().getDeclaredFields();


        for (Field field : fields) {
            result.append("  ");
            try {
                result.append(field.getType() + " ");
                result.append(field.getName());
                result.append(": ");

                result.append(field.get(this));
            } catch (IllegalAccessException ex) {
                System.out.println(ex);
            }
            result.append(NewLine);
        }
        result.append("}");

        return result.toString();

    }
    public void WriteFile() {
    try (PrintWriter out = new PrintWriter("C:\\labs\\outfilename.txt")) {
            out.println(result);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void ReadFile() {
         final String FILENAME = "C:\\labs\\filename.txt";
        BufferedReader br = null;
        FileReader fr = null;

        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }

        } catch (IOException e) {

            e.printStackTrace();

        }
    }


}


