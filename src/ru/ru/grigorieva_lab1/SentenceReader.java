package ru.ru.grigorieva_lab1;
/*ДЗ_Урок15
   Задание 1 (пакет 1)
       Написать программу, читающую текстовый файл.
       Программа должна составлять отсортированный по алфавиту список слов, найденных в файле и сохранять его в файл-результат.
       Найденные слова не должны повторяться. Оптимально приводить их к одному регистру.
       Одно слово в разных падежах это разные слова.
*/
import java.util.Collections;
import java.util.logging.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class SentenceReader {

    public static void main(String[] args)

    {

         final Logger logger = Logger.getLogger(SentenceReader.class.getName());

        File file = new File("C://labs//new.txt");
        File fileOut = new File("C://labs//out.txt");

        List<String> stringList = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            scanner.useDelimiter("\\. ");
            while (scanner.hasNext()) {
                String str = scanner.next();
                stringList.add(str);
                Collections.sort(stringList);
                System.err.println(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
       // return stringList;


    }

}



