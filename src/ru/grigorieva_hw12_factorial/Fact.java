package ru.grigorieva_hw12_factorial;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Fact {

    public static BigInteger fact(int n) {
        BigInteger result = BigInteger.ONE;
        for (int i = 2; i <= n; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }

    public static void main(String[] args) {
        int count = 7;
        Random random = new Random();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            array.add(random.nextInt(20) * 10_000);
        }

        // мой однопоточный факториал
        for (int i = 0; i < count; i++) {
            calc(array, i);
        }


        // с pool of threads
        ThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(count);
        for (int i = 0; i < count; i++) {
            int finalI = i;
            executor.execute(() -> {
                calc(array, finalI);
            });
        }
        executor.shutdown();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                if (executor.awaitTermination(1, TimeUnit.SECONDS)) break;
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }

        }
    }

    private static void calc(List<Integer> array, int finalI) {
        int value = array.get(finalI);
        long time = System.currentTimeMillis();
        System.out.println("Calc factorial for " + value);
        BigInteger result = fact(value);
        System.out.println("factorial " + value + " = " + result);
        System.out.println("Time cal for " + value + " = " + (System.currentTimeMillis() - time));
    }
}